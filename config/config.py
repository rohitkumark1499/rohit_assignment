# -*- coding: utf-8 -*-
"""
Created on Thu Jan  6 12:59:36 2022

@author: hp
"""
import os
from configparser import ConfigParser
root_path=os.path.realpath(os.path.dirname(__file__))
config_url=os.path.join(root_path,"database.ini")

#connecting using sqlalchemy
def connect():
    file = config_url
    config = ConfigParser()
    config.read(file)   
    username = config['postgres']['username']
    password = config['postgres']['password']
    server = config['postgres']['server']
    dbName = config['postgres']['dbName']
    return 'postgresql://'+username+':'+password+'@'+server+'/'+dbName


#connection for api
def connection():
    file = config_url
    config = ConfigParser()
    config.read(file)   
    username = config['postgres']['username']
    password = config['postgres']['password']
    server = config['postgres']['server']
    dbName = config['postgres']['dbName']
    return 'postgresql+psycopg2://'+username+':'+password+'@'+server+'/'+dbName
    
    
    
    #'postgresql+psycopg2://postgres:engineer@localhost/postgres'

#email
def email_credential():
    file = config_url
    config = ConfigParser()
    config.read(file)   
    EMAIL_ADDRESS = config['Email']['Email_id']
    EMAIL_PASSWORD = config['Email']['password']
    receiver = config['Email']['reciever']
    return EMAIL_ADDRESS,EMAIL_PASSWORD,receiver