# -*- coding: utf-8 -*-
"""
Created on Thu Jan  6 15:14:08 2022

@author: hp
"""

import smtplib
from email.message import EmailMessage



def sending_mail(status,EMAIL_ADDRESS,EMAIL_PASSWORD,receiver):
    #recieve = 'rohitkumark1499@gmail.com'

    msg = EmailMessage()
    msg['Subject'] = 'Error Generated!'
    msg['From'] = EMAIL_ADDRESS
    msg['To'] = receiver
    
    msg.set_content('Error: ' + str(status))
    
    
    with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp:
        smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
        smtp.send_message(msg)
        
#sending_mail('error','kumarkrohit1499@gmail.com','Rohit@123')