# -*- coding: utf-8 -*-
"""
Created on Fri Jan  7 10:11:05 2022

@author: hp
"""

import smtplib
from email.message import EmailMessage



def sending_attached_mail(filename,EMAIL_ADDRESS,EMAIL_PASSWORD,receiver):
    #recieve = 'rohitkumark1499@gmail.com'

    msg = EmailMessage()
    msg['Subject'] = filename + ".csv"
    msg['From'] = EMAIL_ADDRESS
    msg['To'] = receiver
    
    msg.set_content('Summary of portfolio deatils')
    
    with open(filename,'rb') as f:
        file_data =f.read()
        file_name = f.name
        msg.add_attachment(file_data,maintype='application',subtype='csv',filename=file_name)
    
    with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp:
        smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
        smtp.send_message(msg)