# -*- coding: utf-8 -*-
"""
Created on Thu Jan  6 16:58:06 2022

@author: hp
"""

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from config.config import connection


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = connection()
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
CORS(app)

class user_details(db.Model):
  __tablename__ = 'user_details'
  user_id = db.Column(db.String(100), primary_key=True)
  first_name = db.Column(db.String(100), nullable=False)
  last_name = db.Column(db.String(100), nullable=False)
  email_id = db.Column(db.String(100), nullable=False)
  phone_no = db.Column(db.String(100), nullable=False)
  active_flag = db.Column(db.Boolean, nullable=False)

  __table_args__ = {'schema':'users'}

  def __repr__(self):
    return "<first_name %r>" % self.first_name