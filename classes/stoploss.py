# -*- coding: utf-8 -*-
"""
Created on Fri Jan  7 09:44:40 2022

@author: hp
"""
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from config.config import connection

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = connection()
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
CORS(app)

class Stoploss(db.Model):
    __tablename__ = 'stoploss_limit'
    user_id = db.Column(db.String, primary_key=True)
    stoploss_limit_1 = db.Column(db.String, nullable=False)
    stoploss_limit_2 = db.Column(db.String, nullable=False)
    create_date = db.Column(db.Date, nullable=False)
    portfolio_id = db.Column(db.Integer, nullable=False)
    stock_id = db.Column(db.Integer, nullable=True)

    __table_args__ = {'schema': 'app_rules'}