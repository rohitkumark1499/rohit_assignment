# -*- coding: utf-8 -*-
"""
Created on Fri Jan  7 09:46:50 2022

@author: hp
"""
from flask import request, jsonify
from utilities.email import sending_mail
from config.config import email_credential
from classes.stoploss import Stoploss , db
from classes.targetprofit import Targetprofit


def create_stop_target(type1, user_id):
    try:
        if type1 == 'stoploss':
            stoploss = Stoploss.query.get(user_id)
          
            # user_data = request.json
            # user_id=user_data['user_id']
            stoploss_limit_1 = request.json['stoploss_limit_1']
            stoploss_limit_2 = request.json['stoploss_limit_2']
            create_date = request.json['create_date']
            portfolio_id = request.json['portfolio_id']
            stock_id = request.json['stock_id']
            
            if stoploss is None:
                  stoploss = Stoploss(user_id=user_id, stoploss_limit_1=stoploss_limit_1, stoploss_limit_2=stoploss_limit_2,
                                      create_date=create_date, portfolio_id=portfolio_id, stock_id=stock_id)
            else:
              # stoploss.user_id = user_id,
              stoploss.stoploss_limit_1 = stoploss_limit_1,
              stoploss.stoploss_limit_2 = stoploss_limit_2,
              stoploss.create_date = create_date
              stoploss.portfolio_id = portfolio_id
              stoploss.stock_id = stock_id
              
            db.session.add(stoploss)
            db.session.commit()
        
            return jsonify({"success": True, "response": "stoploss added"})
        
        elif type1 == 'targetprofit':
            targetprofit = Targetprofit.query.get(user_id)
          
            # user_data = request.json
            # user_id=user_data['user_id']
            target_profit_1 = request.json['target_profit_1']
            target_profit_2 = request.json['target_profit_2']
            create_date = request.json['create_date']
            portfolio_id = request.json['portfolio_id']
            stock_id = request.json['stock_id']
            
            if targetprofit is None:
                targetprofit = Targetprofit(user_id=user_id, target_profit_1=target_profit_1, target_profit_2=target_profit_2,
                                          create_date=create_date, portfolio_id=portfolio_id, stock_id=stock_id)
            else:
                  # stoploss.user_id = user_id,
                  targetprofit.target_profit_1 = target_profit_1,
                  targetprofit.target_profit_1 = target_profit_1,
                  targetprofit.create_date = create_date
                  targetprofit.portfolio_id = portfolio_id
                  targetprofit.stock_id = stock_id
                  
            db.session.add(targetprofit)
            db.session.commit()
      
            return jsonify({"success": True, "response": "target profit added"})
        
    except Exception as ex:
        print(ex)
        email_id,password,receiver=email_credential()
        sending_mail(ex, email_id,password,receiver)
        return ex

              
