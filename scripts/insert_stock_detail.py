# -*- coding: utf-8 -*-
"""
Created on Thu Jan  6 15:08:09 2022

@author: hp
"""
import pandas as pd
from scripts import read_tables as rt
from flask import jsonify
from utilities.email import sending_mail
from config.config import email_credential


def stock_insert():
    try:
        stock_detail_upsert = pd.read_csv('I:\CSV files\stock_details.csv')
        stock_detail = rt.stock_details
        print("insert")
        insert_ticker=set(stock_detail_upsert.ric_ticker.unique().tolist())-set(stock_detail.ric_ticker.unique().tolist())
        print(insert_ticker)
        insert_data =stock_detail_upsert[stock_detail_upsert['ric_ticker'].isin(list(insert_ticker))]
        print(insert_data)
        #insert_data.to_sql("stock_detail", rt.conn, if_exists='append', schema="stock", index=False)
        return jsonify({"success": True, "response": "inserted in stock table"})
    except Exception as ex:
        print(ex)
        email_id,password=email_credential()
        sending_mail(ex, email_id,password)
        return ex
