# -*- coding: utf-8 -*-
"""
Created on Thu Jan  6 16:53:02 2022

@author: hp
"""

from scripts import read_tables as rt
from flask import jsonify
from utilities.email import sending_mail
from config.config import email_credential

def set(value1,value2,value3):
    try:
        portfolio_detail = rt.portfolio_details
        index_value = portfolio_detail[(portfolio_detail['portfolio_id'] == value1) & (portfolio_detail['company_id'] == value2)].index.values.astype(int)[0]
        print(index_value)
        portfolio_detail.at[index_value, 'quantity'] = value3
        print(portfolio_detail)
        return jsonify({"success": True, "response": "value setted"})
    
    except Exception as ex:
        print(ex)
        email_id,password,receiver=email_credential()
        sending_mail(ex, email_id,password,receiver)
        return ex