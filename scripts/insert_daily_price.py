# -*- coding: utf-8 -*-
"""
Created on Thu Jan  6 16:38:00 2022

@author: hp
"""

import pandas as pd
from scripts import read_tables as rt
from flask import jsonify
from utilities.email import sending_mail
from config.config import email_credential

def daily_price_insert():
    try:
        today_price = pd.read_csv('I:\CSV files\daily_price.csv')
        stock_detail = rt.stock_details
        daily_price = rt.daily_price
        print("stock_detail join daily_price:")
        first_join = pd.merge(stock_detail,daily_price)[['company_id',"ric_ticker","open_price","close_price",'date']]
        print(first_join)
        print(first_join.columns)
        print()

        print("stock_details join today_price:")
        second_join = pd.merge(stock_detail,today_price,on="company_id")[['company_id','ric_ticker',"open_price","close_price",'trade_date']]
        print(second_join)
        second_join.rename(columns={'trade_date': 'date'})
        print(second_join.columns)
        print()

        print("insert new price:")
        df_all = first_join.merge(second_join,on=['ric_ticker'],how='right', indicator=True)
        #print(df_all['_merge'] == 'right_only')
        data1=df_all[df_all['_merge'] == 'right_only']
        data1.rename(columns = {'company_id_y':'company_id','open_price_y':'open_price','close_price_y':'close_price'}, inplace = True)
        #d=data1[['company_id_y','open_price_y','closed_price_y','trade_date']]
        data1
        d=data1[['company_id','open_price','close_price','date']]
        print(d)
        #d.to_sql("daily_price", rt.conn, if_exists='append', schema="stock", index=False)
        return jsonify({"success": True, "response": "inserted in daily_price"})
    
    except Exception as ex:
        print(ex)
        email_id,password=email_credential()
        sending_mail(ex, email_id,password)
        return ex