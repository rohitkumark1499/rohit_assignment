# -*- coding: utf-8 -*-
"""
Created on Thu Jan  6 13:01:16 2022

@author: hp
"""

from config.config import connect
import pandas as pd
from sqlalchemy import create_engine

conn = create_engine(connect())


def create_pandas_table(sql_query, database=conn):
    table = pd.read_sql_query(sql_query, database)
    return table

stock_details = create_pandas_table("select * from stock.stock_detail",conn)
daily_price = create_pandas_table("select * from stock.daily_price", conn)

user_detail = create_pandas_table("select * from users.user_details", conn)


user_portfolio = create_pandas_table("select * from portfolio.user_portfolio",conn)
portfolio_details = create_pandas_table("select * from portfolio.portfolio_details",conn)