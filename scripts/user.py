# -*- coding: utf-8 -*-
"""
Created on Thu Jan  6 17:02:36 2022

@author: hp
"""

from flask import request, jsonify
from utilities.email import sending_mail
from config.config import email_credential
from classes.user_details import db, user_details


def create_user():
    try:
        
      user_data = request.json
    
      user_id = user_data['user_id']
      first_name = user_data['first_name']
      last_name = user_data['last_name']
      phone_no = user_data['phone_no']
      email_id = user_data['email_id']
      active_flag = user_data['active_flag']
    
      user = user_details(user_id=user_id,first_name=first_name,last_name=last_name,phone_no=phone_no,email_id=email_id,active_flag=active_flag)
      db.session.add(user)
      db.session.commit()
      
      return jsonify({"success": True, "response": "user_added"})
  
    except Exception as ex:
        print(ex)
        email_id,password,receiver=email_credential()
        sending_mail(ex, email_id,password,receiver)
        return ex