# -*- coding: utf-8 -*-
"""
Created on Thu Jan  6 17:23:07 2022

@author: hp
"""

from flask import request, jsonify
from utilities.email import sending_mail
from config.config import email_credential
from classes.user_details import db, user_details

def get_user():
    try:
        all_users = []
        users = user_details.query.all()
        for user in users:
          results = {
            "user_id": user.user_id,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "phone_no": user.phone_no,
            "email_id": user.email_id,
            "active_flag": user.active_flag, }
          all_users.append(results)
        
        return jsonify(
          {
            "success": True,
            "pets": all_users,
            "total_pets": len(users),
          }
        )
    except Exception as ex:
        print(ex)
        email_id,password=email_credential()
        sending_mail(ex, email_id,password)
        return ex
