# -*- coding: utf-8 -*-
"""
Created on Thu Jan  6 16:30:59 2022

@author: hp
"""

import pandas as pd
from scripts import read_tables as rt
from flask import jsonify
from utilities.email import sending_mail
from config.config import email_credential

def stock_update():
    try:
        stock_detail_upsert = pd.read_csv('I:\CSV files\stock_details.csv')
        stock_detail = rt.stock_details
        print("update")
        update_data=stock_detail_upsert.merge(stock_detail,how='inner',on='ric_ticker')
        print(update_data)
        update_data = update_data[update_data['company_name_x']!=update_data['company_name_y']]
        print(update_data)

        update_data.rename(columns = {'company_id_x':'company_id','company_name_x':'company_name','market_code_y':'market_code'}, inplace = True)
        update = update_data[['company_id','ric_ticker','company_name','market_code']]
        print(update)

        update.to_sql('temp', rt.conn, if_exists='replace', schema='stock_master', index=False)

        rt.conn.execute('delete  from stock_master.stock_detail using stock_master.temp where stock_master.stock_detail.company_id = stock_master.temp.company_id  ')

        update.to_sql('stock_detail', rt.conn, schema='stock_master', if_exists='append', index=False)
        
        return jsonify({"success": True, "response": "updated in stock table"})
    
    except Exception as ex:
        print(ex)
        email_id,password,receiver=email_credential()
        sending_mail(ex, email_id,password,receiver)
        return ex