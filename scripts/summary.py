# -*- coding: utf-8 -*-
"""
Created on Fri Jan  7 10:13:23 2022

@author: hp
"""
from scripts import read_tables as rt
import pandas as pd
from utilities.email import sending_mail
from config.config import email_credential
from flask import jsonify
from utilities.attachment_mail import sending_attached_mail

def summary_detail(i):
    try:
        portfolio_detail = rt.portfolio_details
        daily_price = rt.daily_price
        stock_detail = rt.stock_details
        b=portfolio_detail[portfolio_detail["portfolio_id"] == i]
        #print(b)
        c=b["company_id"].tolist()

        column_names = ["company_id", "open_price", "close_price","date"]

        temp1 = pd.DataFrame(columns = column_names)


        for i in c:
            a = daily_price[daily_price['company_id'] == i]['date'].max()
            d=daily_price[(daily_price["date"] == a) & (daily_price["company_id"] == i)]
            #print(d)
            temp1=pd.concat([temp1,d],ignore_index=True)

        #print(temp1)

        join = pd.merge(temp1,b,on="company_id")[["user_id","portfolio_id","company_id","price","close_price","date"]]
        #print(join.columns)
        #print(join)
        join['percentage_change'] = ((join['price']/join['close_price'])-1)*100
        aggregate=join['percentage_change'].sum()
        #print(join)

        if aggregate < -10:
            print("your portfolio is performing very poor")
        elif aggregate > -10 and aggregate < 0:
            print("your portfolio is performing poor")
        elif aggregate > 0 and aggregate < 10:
            print("your portfolio is performing moderated")
        elif aggregate > 10 and aggregate < 30:
            print("your portfolio is performing good")
        else:
            print("your portfolio is performing excelent")

        join1 = pd.merge(join,stock_detail,on="company_id")[["company_name","percentage_change"]]
        print(join1.columns)
        print(join1)
        
        filename = "percentage_change.csv"
        #join1.to_csv(filename,index=False)
        email_id,password,receiver=email_credential()
        sending_attached_mail(filename, email_id,password,receiver)
        #return "success"
        return jsonify({"success": True, "response": "process successful"})

    except Exception as ex:
        print(ex)
        email_id,password,receiver=email_credential()
        sending_mail(ex, email_id,password,receiver)
        return ex
