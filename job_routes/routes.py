# -*- coding: utf-8 -*-
"""
Created on Thu Jan  6 15:07:17 2022

@author: hp
"""

from flask import Flask
from flask_cors import cross_origin, CORS
from flask_sqlalchemy import SQLAlchemy
from config.config import connection
from services.service import stock_detail_insert
from services.service import stock_detail_update
from services.service import dailyprice_insert
from services.service import getmethod
from services.service import setmethod
from services.service import createuser
from services.service import getuser
from services.service import create_st
from services.service import summary_details
from services.service import exportapi




app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = connection()
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
CORS(app)


@cross_origin
@app.route('/insertstock',methods=['GET'])
def dynamic_page1():
    try:
        return stock_detail_insert()
    except Exception as ex:
        print(ex)
        return ex

@cross_origin
@app.route('/updatestock',methods=['GET'])
def dynamic_page2():
    try:
        return stock_detail_update()
    
    except Exception as ex:
        print(ex)
        return ex


@cross_origin
@app.route('/insertprice',methods=['GET'])
def dynamic_page3():
    try:
        return dailyprice_insert()
    
    except Exception as ex:
        print(ex)
        return ex


@cross_origin
@app.route('/getvalue',methods=['GET'])
def dynamic_page4():
    try:
        return getmethod("portfolio_id", 6)
    
    except Exception as ex:
        print(ex)
        return ex


@cross_origin
@app.route('/setvalue',methods=['GET'])
def dynamic_page5():
    try:
        return setmethod(6,53,16)
    
    except Exception as ex:
        print(ex)
        return ex


@cross_origin()
@app.route('/users', methods=['POST'])
def dynamic_page6():
    try:
        return createuser()
    except Exception as ex:
        print(ex)
        

@cross_origin()
@app.route('/getusers', methods = ['GET'])
def dynamic_page7():
    try:
        return getuser()
    except Exception as ex:
        print(ex)
        

@cross_origin()
@app.route('/app_rules/<string:type1>/<string:user_id>', methods=['PATCH'])
def dynamic_page8(type1,user_id):
    try:
        return create_st(type1, user_id)
    except Exception as ex:
        print(ex)

@cross_origin
@app.route('/summary')
def dynamic_page9():
    try:
        return summary_details(16)   
    except Exception as ex:
        print(ex)


@cross_origin
@app.route('/export')
def dynamic_page10():
    try:
        return exportapi()  
    except Exception as ex:
        print(ex)









if __name__ == '__main__':
    app.run()