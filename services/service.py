# -*- coding: utf-8 -*-
"""
Created on Thu Jan  6 15:22:55 2022

@author: hp
"""

from scripts.insert_stock_detail import stock_insert
from scripts.update_stock_details import stock_update
from scripts.insert_daily_price import daily_price_insert
from scripts.get_method import get
from scripts.set_method import set
from scripts.user import create_user
from scripts.getuser import get_user
from scripts.stop_and_target import create_stop_target
from scripts.summary import summary_detail
from classes.custom_exception import EnterValidEx
from scripts.export_api import multi



def stock_detail_insert():
    try:
        res = stock_insert()
        return res
    except Exception as ex:
        print("exception occured in services stock_detail_insert(): ",ex)
        

def stock_detail_update():
    try:
        res = stock_update()
        return res
    except Exception as ex:
        print("exception occured in services stock_detail_update(): ",ex)
        

def dailyprice_insert():
    try:
        res = daily_price_insert()
        return res
    except Exception as ex:
        print("exception occured in services dailyprice_insert(): ",ex)


def getmethod(col_name,value):
    try:
        res = get(col_name, value)
        return res
    except Exception as ex:
        print("exception occured in services getmethod(): ",ex)
        

def setmethod(value1,value2,value3):
    try:
        res = set(value1, value2, value3)
        return res
    except Exception as ex:
        print("exception occured in service setmethod(): ",ex)
        
        
def createuser():
    try:
        res = create_user()
        return res
    except Exception as ex:
        print("exception occured in service createuser(): ",ex)
        
        
def getuser():
    try:
        res = get_user()
        return res
    except Exception as ex:
        print("exception occured in service getuser(): ",ex)
        
def create_st(type1,user_id):
    try:
        res = create_stop_target(type1, user_id)
        return res
    except Exception as ex:
        print("exception occured in service create_st(): ",ex)
        
def summary_details(i):
    try:
        res = summary_detail(i)
        #return res
        raise EnterValidEx
    except EnterValidEx:
        print("exception occured in service create_st() give return statement ")

def exportapi():
    try:
        res = multi()
        return res
    except Exception as ex:
        print("exception occured in service exportapi(): ",ex)        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        